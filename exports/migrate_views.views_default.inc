<?php

function migrate_views_views_default_views() {
  $view = new view;
  $view->name = 'comments_recent';
  $view->description = 'Contains a block and a page to list recent comments; the block will automatically link to the page, which displays the comment body as well as a link to the node.';
  $view->tag = 'default';
  $view->base_table = 'comments';
  $view->human_name = '';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'nid' => array(
      'id' => 'nid',
      'table' => 'comments',
      'field' => 'nid',
      'label' => 'Node',
      'required' => FALSE,
    ),
  ));
  $handler->override_option('fields', array(
    'subject' => array(
      'id' => 'subject',
      'table' => 'comments',
      'field' => 'subject',
      'label' => '',
      'link_to_comment' => 1,
      'relationship' => 'none',
    ),
    'timestamp' => array(
      'id' => 'timestamp',
      'table' => 'comments',
      'field' => 'timestamp',
      'label' => '',
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'timestamp' => array(
      'id' => 'timestamp',
      'table' => 'comments',
      'field' => 'timestamp',
      'order' => 'DESC',
      'granularity' => 'second',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
      'operator' => '=',
      'value' => '',
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'relationship' => 'nid',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'role' => array(),
    'perm' => 'access comments',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Recent comments');
  $handler->override_option('items_per_page', 5);
  $handler->override_option('use_more', 1);
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'type' => 'ul',
  ));
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->override_option('fields', array(
    'title' => array(
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'label' => 'Reply to',
      'relationship' => 'nid',
      'link_to_node' => 1,
    ),
    'timestamp' => array(
      'id' => 'timestamp',
      'table' => 'comments',
      'field' => 'timestamp',
      'label' => '',
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'relationship' => 'none',
    ),
    'subject' => array(
      'id' => 'subject',
      'table' => 'comments',
      'field' => 'subject',
      'label' => '',
      'link_to_comment' => 1,
      'relationship' => 'none',
    ),
    'comment' => array(
      'id' => 'comment',
      'table' => 'comments',
      'field' => 'comment',
      'label' => '',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('items_per_page', 25);
  $handler->override_option('use_pager', '1');
  $handler->override_option('row_options', array(
    'inline' => array(
      'title' => 'title',
      'timestamp' => 'timestamp',
    ),
    'separator' => '&nbsp;',
  ));
  $handler->override_option('path', 'comments/recent');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->override_option('block_description', 'Recent comments view');
  $handler->override_option('block_caching', -1);
//  $views['comments_recent'] = $view;

  $view = new view;
  $view->name = 'frontpage';
  $view->description = 'Emulates the default Drupal front page; you may set the default home page path to this view to make it your front page.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = '';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('sorts', array(
    'sticky' => array(
      'id' => 'sticky',
      'table' => 'node',
      'field' => 'sticky',
      'order' => 'DESC',
    ),
    'created' => array(
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'order' => 'DESC',
      'relationship' => 'none',
      'granularity' => 'second',
    ),
  ));
  $handler->override_option('filters', array(
    'promote' => array(
      'id' => 'promote',
      'table' => 'node',
      'field' => 'promote',
      'operator' => '=',
      'value' => '1',
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
    ),
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'operator' => '=',
      'value' => '1',
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'role' => array(),
    'perm' => 'access content',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'my title');
  $handler->override_option('header', 'my header');
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 1);
  $handler->override_option('footer', 'my footer');
  $handler->override_option('footer_format', '1');
  $handler->override_option('footer_empty', 0);
  $handler->override_option('empty', 'my empty text');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 12);
  $handler->override_option('offset', 2);
  $handler->override_option('use_pager', '1');
  $handler->override_option('use_more', 1);
  $handler->override_option('use_more_always', 1);
  $handler->override_option('row_plugin', 'node');
  $handler->override_option('row_options', array(
    'teaser' => 1,
    'links' => 1,
  ));
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->override_option('path', 'frontpage');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('feed', 'Feed', 'feed');
  $handler->override_option('title', 'Front page feed');
  $handler->override_option('style_plugin', 'rss');
  $handler->override_option('style_options', array(
    'mission_description' => 1,
    'description' => '',
  ));
  $handler->override_option('row_plugin', 'node_rss');
  $handler->override_option('row_options', array(
    'item_length' => 'default',
  ));
  $handler->override_option('path', 'rss.xml');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('displays', array(
    'default' => 'default',
    'page' => 'page',
  ));
  $handler->override_option('sitename_title', '1');
  $views['frontpage'] = $view;

  return $views;
}

