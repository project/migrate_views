<?php

namespace Drupal\migrate_views;

class BaseTableMapping {

  public function getValue($old_table) {
    $map = [
      'node' => 'node_field_data',
      'comments' => 'comment_field_data',
    ];
    if (isset($map[$old_table])) {
      return $map[$old_table];
    }
    else {
      print_r($old_table . "\n");
    }
    return $old_table;
  }

}
