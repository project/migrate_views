<?php

namespace Drupal\migrate_views;

class FieldMapping {

  public function getValue($table, $field) {
    $map = [];

    $map['comments'][$field] = 'changed';
    $map['comment_field_data'][$field] = 'changed';

    if (isset($map[$table][$field])) {
      $field = $map[$table][$field];
    }

    return $field;
  }

}
