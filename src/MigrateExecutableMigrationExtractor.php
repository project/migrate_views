<?php

namespace Drupal\migrate_views;

use Drupal\migrate\MigrateExecutableInterface;

class MigrateExecutableMigrationExtractor {

  public function extractMigration(MigrateExecutableInterface $executable) {
    $function = function () {
      return $this->migration;
    };
    $function = \Closure::bind($function, $executable, $executable);
    /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
    $migration = $function();
    return $migration;
  }

  public static function instance() {
    return new static();
  }

}
