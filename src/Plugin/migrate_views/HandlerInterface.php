<?php

namespace Drupal\migrate_views\Plugin\migrate_views;
use Drupal\migrate\Plugin\MigrateProcessInterface;

/**
 * Handler level operations ...
 */
interface HandlerInterface extends MigrateProcessInterface {

}
