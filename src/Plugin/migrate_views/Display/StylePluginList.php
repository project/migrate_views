<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;

/**
 * @Plugin(
 *   id = "style_plugin_list",
 *   depends = "style_plugin",
 * )
 */
class StylePluginList extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['style']) && $value['style']['type'] == 'list') {
      $value['style']['type'] = 'html_list';
    }
    return $value;
  }

}
