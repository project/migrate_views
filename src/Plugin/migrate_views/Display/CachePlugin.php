<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "cache_plugin",
 * )
 */
class CachePlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['cache'])) {
      $cache = $value['cache'];
  
      $type = $cache['type'];
      unset($cache['type']);
      $options = $cache;

      $value['cache'] = [
        'type' => $type,
        'options' => $options,
      ];
    }
    return $value;
  }

}
