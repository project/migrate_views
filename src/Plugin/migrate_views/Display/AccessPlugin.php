<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "access_plugin",
 * )
 */
class AccessPlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['access'])) {
      $access = $value['access'];

      $type = $access['type'];
      unset($access['type']);
      $options = $access;

      $value['access'] = [
        'type' => $type,
        'options' => $options,
      ];
    }
    return $value;
  }

}
