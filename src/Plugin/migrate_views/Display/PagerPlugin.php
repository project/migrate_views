<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "pager_plugin",
 * )
 */
class PagerPlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['use_pager'])) {
      $use_pager = $value['use_pager'];
      $items_per_page = isset($value['items_per_page']) ? $value['items_per_page'] : NULL;
      $offset = !empty($value['offset']) ? $value['offset'] : 0;
      $pager_element = !empty($value['pager_element']) ? $value['pager_element'] : 0;

      if ($use_pager && !isset($items_per_page)) {
        $value['pager'] = [
          'type' => 'full',
          'options' => [
            'offset' => $offset,
            'items_per_page' => 10,
            'pager_element' => $pager_element,
          ],
        ];
      }
      elseif ($use_pager && isset($items_per_page)) {
        $value['pager'] = [
          'type' => 'full',
          'options' => [
            'offset' => $offset,
            'items_per_page' => $items_per_page,
            'pager_element' => $pager_element,
          ],
        ];
      }
      elseif (!$use_pager && isset($items_per_page) && $items_per_page == 0 && $offset == 0) {
        $value['pager'] = [
          'type' => 'none',
        ];
      }
      elseif (!$use_pager && isset($items_per_page)) {
        $value['pager'] = [
          'type' => 'some',
          'options' => [
            'offset' => $offset,
            'items_per_page' => $items_per_page,
          ],
        ];
      }
    }

    unset($value['use_pager'], $value['items_per_page'], $value['offset'], $value['pager_element']);
    return $value;
  }

}
