<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "row_plugin",
 * )
 */
class RowPlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['row_plugin'])) {
      $type = $value['row_plugin'];
      $options = isset($value['row_options']) ? $value['row_options'] : [];
      unset($value['row_plugin'], $value['row_options']);

      $value['row'] = [
        'type' => $type,
        'options' => $options,
      ];
    }
    return $value;
  }

}
