<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\MigrateExecutableMigrationExtractor;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;


/**
 * @Plugin(
 *   id = "area_handlers",
 * )
 */
class AreaHandlers extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (!empty($value['header']) && !is_array($value['header'])) {
      $value['header'] = ['text' => [
        'content' => $value['header'],
        'format' => $this->mapFormat($value['header_format'], $migrate_executable, $row, $destination_property),
        'empty' => !empty($value['header_empty']),
        'id' => 'text',
        'plugin_id' => 'text',
        'table' => 'views',
        'field' => 'area'
      ]];
      unset($value['header_format'], $value['header_empty']);
    }
    if (!empty($value['footer']) && !is_array($value['footer'])) {
      $value['footer'] = ['text' => [
        'content' => $value['footer'],
        'format' => $this->mapFormat($value['footer_format'], $migrate_executable, $row, $destination_property),
        'empty' => !empty($value['footer_empty']),
        'id' => 'text',
        'plugin_id' => 'text',
        'table' => 'views',
        'field' => 'area'
      ]];
      unset($value['footer_format'], $value['footer_empty']);
    }
    if (!empty($value['empty']) && !is_array($value['empty'])) {
      $value['empty'] = ['text' => [
        'content' => $value['empty'],
        'format' => $this->mapFormat($value['empty_format'], $migrate_executable, $row, $destination_property),
        'id' => 'text',
        'plugin_id' => 'text',
        'table' => 'views',
        'field' => 'area'
      ]];
      unset($value['empty_format']);
    }
    return $value;
  }

  protected function mapFormat($format, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $process_plugin_manager */
    $process_plugin_manager = \Drupal::service('plugin.manager.migrate.process');
    /** @var \Drupal\migrate\Plugin\migrate\process\Migration $migration_plugin */
    $configuration = [
      'migration' => 'd6_filter_format',
    ];
    $migration = MigrateExecutableMigrationExtractor::instance()->extractMigration($migrate_executable);
    $migration_plugin = $process_plugin_manager->createInstance('migration', $configuration, $migration);
    try {
      $result = $migration_plugin->transform($format, $migrate_executable, $row, $destination_property);
    }
    catch (\Exception $e) {
      trigger_error($e->getMessage());
    }

    if (empty($result)) {
      switch ($format) {
        case 0:
          $result = 'basic_html';
          break;
        case 1:
          $result = 'plain_text';
          break;
        default:
        case 2:
          $result = 'basic_html';
          break;
      }
    }

    return $result;
  }

}
