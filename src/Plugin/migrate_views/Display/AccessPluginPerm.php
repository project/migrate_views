<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "access_plugin_perm",
 *   depends = "access_plugin",
 * )
 */
class AccessPluginPerm extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['access']) && $value['access']['type'] == 'perm') {
      // @todo Maybe choose 'access content' by default?
      $value['access']['options'] += ['perm' => []];
    }
    return $value;
  }

}
