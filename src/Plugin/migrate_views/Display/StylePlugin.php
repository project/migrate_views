<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;

/**
 * @Plugin(
 *   id = "style_plugin",
 * )
 */
class StylePlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['style_plugin'])) {
      $type = $value['style_plugin'];
      $options = isset($value['style_options']) ? $value['style_options'] : [];

      $value['style'] = [
        'type' => $type,
        'options' => $options,
      ];
    }
    unset($value['style_plugin'], $value['style_options']);

    return $value;
  }

}
