<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "row_plugin_node",
 *   depends = "row_plugin",
 * )
 */
class RowPluginNode extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['row']['type']) && $value['row']['type'] == 'node') {
      $value['row']['type'] = 'entity:node';

      // The default which is teaser isn't exported so if there is no
      // build_mode, set it to teaser.
      if (!isset($value['row']['options']['build_mode'])) {
        $value['row']['options']['build_mode'] = 'teaser';
      }
      unset($value['row']['options']['teaser']);
      unset($value['row']['options']['links']);
    }
    return $value;
  }

}
