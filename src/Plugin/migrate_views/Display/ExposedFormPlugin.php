<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Display;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\DisplayInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "exposed_form_plugin",
 * )
 */
class ExposedFormPlugin extends ProcessPluginBase implements DisplayInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return $value;
  }

}
