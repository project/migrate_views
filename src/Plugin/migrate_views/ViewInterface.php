<?php

namespace Drupal\migrate_views\Plugin\migrate_views;
use Drupal\migrate\Plugin\MigrateProcessInterface;

/**
 * View level operations ...
 */
interface ViewInterface extends MigrateProcessInterface {

}
