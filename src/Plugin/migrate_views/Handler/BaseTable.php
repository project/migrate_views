<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Handler;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\BaseTableMapping;
use Drupal\migrate_views\Plugin\migrate_views\HandlerInterface;

/**
 * @Plugin(
 *   id = "base_table"
 * )
 */
class BaseTable extends ProcessPluginBase implements HandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['table'])) {
      $value['table'] = (new BaseTableMapping())->getValue($value['table']);
    }
    return $value;
  }

}
