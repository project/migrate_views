<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Handler;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\FieldMapping;
use Drupal\migrate_views\Plugin\migrate_views\HandlerInterface;

/**
 * @Plugin(
 *   id = "base_field",
 *   depends = "base_table",
 * )
 */
class BaseField extends ProcessPluginBase implements HandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['table']) && isset($value['field'])) {
      $value['field'] = (new FieldMapping())->getValue($value['table'], $value['field']);
    }
    return $value;
  }

}
