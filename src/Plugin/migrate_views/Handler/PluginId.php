<?php

namespace Drupal\migrate_views\Plugin\migrate_views\Handler;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\HandlerInterface;
use Drupal\views\Views;
use Drupal\views\ViewsData;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @Plugin(
 *   id = "plugin_id",
 *   depends = "base_table,base_field",
 * )
 */
class PluginId extends ProcessPluginBase implements HandlerInterface, ContainerFactoryPluginInterface {

  /**
   * The views data.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsData $views_data) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->viewsData = $views_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('views.views_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (isset($value['table']) && isset($value['field'])) {
      $table_data = $this->viewsData->get($value['table']);
      if (!isset($table_data[$value['field']])) {
        throw new \Exception("Field {$value['field']} in table {$value['table']} doesn't exist.");
      }
      array_pop($destination_property);
      $plural_type = array_pop($destination_property);
      $type = $this->getTypeFromPluralType($plural_type);

      $field_data = $table_data[$value['field']];
      $value['plugin_id'] = $field_data[$type]['id'];
    }
    return $value;
  }

  protected function getTypeFromPluralType($plural_type) {
    $types = Views::getHandlerTypes();
    $type_ids = array_keys($types);
    $map = array_combine($type_ids, array_map(function($type_info) use ($types) {
      return $type_info['plural'];
    }, $types));
    $map_flipped = array_flip($map);
    return $map_flipped[$plural_type];
  }

}
