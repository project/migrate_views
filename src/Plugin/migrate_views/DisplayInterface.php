<?php

namespace Drupal\migrate_views\Plugin\migrate_views;
use Drupal\migrate\Plugin\MigrateProcessInterface;

/**
 * Display level operations ...
 */
interface DisplayInterface extends MigrateProcessInterface {

}
