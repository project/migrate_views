<?php

namespace Drupal\migrate_views\Plugin\migrate_views\View;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_views\Plugin\migrate_views\ViewInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * @Plugin(
 *   id = "base_table"
 * )
 */
class BaseTable extends ProcessPluginBase implements ViewInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $a = 123;
  }

}
