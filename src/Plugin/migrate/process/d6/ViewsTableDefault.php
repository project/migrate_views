<?php

namespace Drupal\migrate_views\Plugin\migrate\process\d6;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\ViewsData;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Handles the default plugin id generation.
 *
 * @MigrateProcessPlugin(
 *   id = "views_table_default"
 * )
 */
class ViewsTableDefault extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The views data object, containing the cached information.
   *
   * @var \Drupal\views\ViewsData
   */
  protected $viewsData;

  /**
   * Construct the views display transformation plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\views\ViewsData $views_data
   *   The views data cache.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityManagerInterface $entity_manager, ViewsData $views_data) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
    $this->viewsData = $views_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('views.views_data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($config, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // Grab the required config.
    $views_data = $this->viewsData->get();
    $d6_table = $config['d6_table'];
    $level2_key = $config['level2_key'];
    $d8_plugin_type = $config['d8_plugin_type'];
    $table_mapping_array = $config['table_mapping_array'];

    // Initialise to an empty plugin id.
    $d8_plugin_id = '';

    $d8_table = isset($table_mapping_array[$d6_table]) ? $table_mapping_array[$d6_table] : $d6_table;
    if (isset($views_data[$d8_table])) {
      if (isset($views_data[$d8_table]['table']['entity type'])) {

        /** @var \Drupal\Core\Entity\Sql\TableMappingInterface $table_mapping */
        $table_mapping = $this->entityManager
          ->getStorage($views_data[$d8_table]['table']['entity type'])
          ->getTableMapping();
        foreach ($table_mapping->getTableNames() as $d8_table) {
          foreach ($table_mapping->getFieldNames($d8_table) as $d8_field_name) {
            $columns = $table_mapping->getColumnNames($d8_field_name);

            // Make sure that the field is in the column list and also that
            // it is currently set in the $views_data.
            if (in_array($level2_key, $columns) && isset($views_data[$d8_table][$level2_key][$d8_plugin_type]['id'])) {
              $d8_plugin_id = $views_data[$d8_table][$level2_key][$d8_plugin_type]['id'];
              break 2;
            }
          }
        }
      }
      else {
        $d8_plugin_id = $views_data[$d8_table][$level2_key][$d8_plugin_type]['id'];
      }
    }

    return $d8_plugin_id;
  }

}
