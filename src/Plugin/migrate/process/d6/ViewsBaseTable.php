<?php

namespace Drupal\migrate_views\Plugin\migrate\process\d6;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\BaseTableMapping;

/**
 * @MigrateProcessPlugin(
 *   id = "views_base_table"
 * )
 */
class ViewsBaseTable extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = (new BaseTableMapping())->getValue($value);
    return $value;
  }

}
