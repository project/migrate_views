<?php

namespace Drupal\migrate_views\Plugin\migrate\process\d6;

use Drupal\Component\Graph\Graph;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigratePluginManager;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_views\MigrateViewsPluginManager;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles the displays array exported from views.
 *
 * @MigrateProcessPlugin(
 *   id = "views_displays",
 *   handle_multiples = true,
 * )
 *
 * @todo Deal with display.defaults
 */
class ViewsDisplays extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The migration.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * @var \Drupal\migrate\Plugin\MigratePluginManager
   */
  protected $processPluginManager;

  /**
   * @var \Drupal\migrate\MigrateExecutable
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\migrate\Row
   */
  protected $row;

  /**
   * @var string
   */
  protected $destinationProperty;


  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $viewsPluginManager;

  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $displayPluginManager;

  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $handlerPluginManager;

  /**
   * Construct the views display transformation plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration entity.
   * @param \Drupal\migrate\Plugin\MigratePluginManager $process_plugin_manager
   *   The plugin manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, MigratePluginManager $process_plugin_manager, MigrateViewsPluginManager $viewsPluginManager, MigrateViewsPluginManager $displayPluginManager, MigrateViewsPluginManager $handlerPluginManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->processPluginManager = $process_plugin_manager;
    $this->migration = $migration;

    $this->viewsPluginManager = $viewsPluginManager;
    $this->displayPluginManager = $displayPluginManager;
    $this->handlerPluginManager = $handlerPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('plugin.manager.migrate.process'),
      $container->get('plugin.manager.migrate_views.migrate_views_view'),
      $container->get('plugin.manager.migrate_views.migrate_views_display'),
      $container->get('plugin.manager.migrate_views.migrate_views_handler')
    );
  }

  protected function getPluginsSortedAsGraph(PluginManagerInterface $plugin_manager) {
    $definitions = $plugin_manager->getDefinitions();
    $plugin_ids = array_keys($definitions);
    $display_plugins = array_map(function ($plugin_id) use ($plugin_manager) {
      return $plugin_manager->createInstance($plugin_id);
    }, array_combine($plugin_ids, $plugin_ids));

    $graph = [];
    array_walk($display_plugins, function (PluginInspectionInterface $plugin, $plugin_Id) use (&$graph) {
      $definition = $plugin->getPluginDefinition();

      $graph[$plugin_Id] = [];
      if (isset($definition['depends'])) {
        $dependencies = explode(',', $definition['depends']);
        foreach ($dependencies as $dependency) {
          $graph[$plugin_Id]['edges'][$dependency] = 1;
        }
      }
    });

    $graph = new Graph($graph);
    $graph = $graph->searchAndSort();
    uasort($graph, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    $graph = array_reverse($graph);
    $graph = array_combine(array_keys($graph), array_map(function ($item, $plugin_id) use ($plugin_manager) {
      $item['plugin_instance'] = $plugin_manager->createInstance($plugin_id);
      return $item;
    }, $graph, array_keys($graph)));

    return $graph;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($displays, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $return = [];

    $handler_plugins_graph = $this->getPluginsSortedAsGraph($this->handlerPluginManager);
    $display_graph = $this->getPluginsSortedAsGraph($this->displayPluginManager);

    foreach ($displays as $display_id => $display) {
      /** @var \Drupal\migrate_views\Plugin\migrate_views\DisplayInterface $display_plugin */
      $d8_display_options = $display['display_options'];
      foreach ($display_graph as $display_plugin_id => $info) {
        $display_plugin = $info['plugin_instance'];
        $d8_display_options = $display_plugin->transform($d8_display_options, $migrate_executable, $row, ['display', $display_id]);
      }
      $display['display_options'] = $d8_display_options;

      foreach (Views::getHandlerTypes() as $type => $info) {
        $plural_type = $info['plural'];
        if (empty($display['display_options'][$plural_type])) {
          continue;
        }
        $d8_display_options[$plural_type] = $display['display_options'][$plural_type];
        if (!empty($d8_display_options[$plural_type])) {
          foreach ($d8_display_options[$plural_type] as $id => $info) {
            /** @var \Drupal\migrate_views\Plugin\migrate_views\HandlerInterface $handler_plugin */
            foreach ($handler_plugins_graph as $handler_plugin_id => $plugin_info) {
              $handler_plugin = $plugin_info['plugin_instance'];
              $info = $handler_plugin->transform($info, $migrate_executable, $row, ['display', $display_id, 'display_options', $plural_type, $id]);
            }
            $d8_display_options[$plural_type][$id] = $info;
          }
        }
      }

      // The display keeps it's top level keys such as type, title and id but the
      // display options are overwritten by the newly created options from the
      // migrate views process plugins.
      $display['display_options'] = $d8_display_options;
      $return[$display_id] = $display;
    }
    return $return;
  }

}
