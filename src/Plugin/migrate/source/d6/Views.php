<?php

namespace Drupal\migrate_views\Plugin\migrate\source\d6 {

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;

/**
 * A source for Drupal 6 3.x views.
 *
 * @MigrateSource(
 *   id = "d6_views",
 *   source_provider = "views"
 * )
 */
class Views extends SourcePluginBase {

  /**
   * The views, converted to arrays.
   *
   * Store in static property to avoid rescanning if used more than once.
   *
   * @var \view[]
   */
  static $views = [];

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    return new \ArrayIterator($this->getViews());
  }

  /**
   * @return \view[]
   */
  protected function getViews() {
    $path = $this->configuration['views_path'];
    if (!isset(static::$views[$path])) {
      static::$views[$path] = [];

      // @todo figure that out
//      foreach (glob(__DIR__ . '../../../../../exports/*.views_default.inc') as $file) {

      $files = ['modules/migrate_views/exports/migrate_views.views_default.inc'];
      foreach ($files as $file) {
        require $file;
        // Grab the prefix which is in the format module.views_default.inc.
        $prefix = explode('.', basename($file))[0];
        $function = $prefix . '_views_default_views';
        if (function_exists($function)) {
          static::$views[$path] += array_map('get_object_vars', $function());
        }
      }
    }
    return static::$views[$path];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'name' => $this->t('The machine name of the view'),
      'description' => $this->t('The view description'),
      'tag' => $this->t('View tags?'),
      'base_table' => $this->t('The base table for querying data.'),
      'human_name' => $this->t('The human readable name for the view.'),
      'core' => $this->t('The core version.'),
      'api_version' => $this->t('The API version'),
      'disabled' => $this->t('Disabled?'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->getViews());
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['name']['type'] = 'string';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return 'Views Source';
  }

}

}

namespace {
  class view {

    public $name;
    public $description;
    public $tag;
    public $base_table;
    public $human_name;
    public $core;
    public $api_version;
    public $disabled;

    public function new_display($type = 'page', $title = NULL, $id = NULL) {
      $display = new views_plugin_display($type, $title, $id);
      // @todo Positions.
      $this->displays[$display->id] = [
        'display_plugin' => $display->type,
        'display_title' => $display->title,
        'id' => $display->id,
        'display_options' => &$display->display->display_options,
      ];
      return $display;
    }
  }

  class views_plugin_display {
    public $type;
    public $title;
    public $id;
    public $display;
    public $options = [];

    public function __construct($type, $title, $id) {
      $this->type = $type;
      $this->title = $title;
      $this->id = $id;
      $this->display = new \stdClass;
      $this->display->display_options = [];
    }

    /**
     * Set an option and force it to be an override.
     */
    function override_option($option, $value) {
      $this->set_override($option, FALSE);
      $this->set_option($option, $value);
    }

    function defaultable_sections($section = NULL) {
      $sections = [
        'access' => ['access'],
        'cache' => ['cache'],
        'title' => ['title'],
        'css_class' => ['css_class'],
        'header' => ['header', 'header_format', 'header_empty'],
        'footer' => ['footer', 'footer_format', 'footer_empty'],
        'empty' => ['empty', 'empty_format'],
        'use_ajax' => ['use_ajax'],
        'items_per_page' => ['items_per_page', 'offset', 'use_pager', 'pager_element'],
        'use_pager' => ['items_per_page', 'offset', 'use_pager', 'pager_element'],
        'use_more' => ['use_more', 'use_more_always', 'use_more_text'],
        'link_display' => ['link_display'],
        'distinct' => ['distinct'],
        'exposed_block' => ['exposed_block'],

        // Force these to cascade properly.
        'style_plugin' => ['style_plugin', 'style_options', 'row_plugin', 'row_options'],
        'style_options' => ['style_plugin', 'style_options', 'row_plugin', 'row_options'],
        'row_plugin' => ['style_plugin', 'style_options', 'row_plugin', 'row_options'],
        'row_options' => ['style_plugin', 'style_options', 'row_plugin', 'row_options'],

        // These guys are special
        'relationships' => ['relationships'],
        'fields' => ['fields'],
        'sorts' => ['sorts'],
        'arguments' => ['arguments'],
        'filters' => ['filters'],
      ];
      if ($section) {
        if (!empty($sections[$section])) {
          return $sections[$section];
        }
      }
      else {
        return $sections;
      }
    }

    /**
     * Flip the override setting for the given section.
     */
    function set_override($section, $new_state = NULL) {
      $options = $this->defaultable_sections($section);
      if (!$options) {
        return;
      }

      if (!isset($new_state)) {
        $new_state = empty($this->options['defaults'][$section]);
      }

      // For each option that is part of this group, fix our settings.
      foreach ($options as $option) {
        if ($new_state) {
          // Revert to defaults.
          unset($this->options[$option]);
          unset($this->display->display_options[$option]);
        }
        else {
          // copy existing values into our display.
          $this->options[$option] = $this->get_option($option);
          $this->display->display_options[$option] = $this->options[$option];
        }
        $this->options['defaults'][$option] = $new_state;
        $this->display->display_options['defaults'][$option] = $new_state;
      }
    }


    /**
     * Determine if a given option is set to use the default display or the
     * current display
     *
     * @return
     *   TRUE for the default display
     */
    function is_defaulted($option) {
      return !$this->is_default_display() && !empty($this->default_display) && !empty($this->options['defaults'][$option]);
    }

    /**
     * Intelligently get an option either from this display or from the
     * default display, if directed to do so.
     */
    function get_option($option) {
      if ($this->is_defaulted($option)) {
        return $this->default_display->get_option($option);
      }

      if (array_key_exists($option, $this->options)) {
        return $this->options[$option];
      }
    }

    /**
     * Determine if this display is the 'default' display which contains
     * fallback settings
     */
    function is_default_display() {
      return $this->id == 'master';
    }

    /**
     * Intelligently set an option either from this display or from the
     * default display, if directed to do so.
     */
    function set_option($option, $value) {
      if ($this->is_defaulted($option)) {
        return $this->default_display->set_option($option, $value);
      }

      // Set this in two places: On the handler where we'll notice it
      // but also on the display object so it gets saved. This should
      // only be a temporary fix.
      $this->display->display_options[$option] = $value;
      return $this->options[$option] = $value;
    }

  }

}
