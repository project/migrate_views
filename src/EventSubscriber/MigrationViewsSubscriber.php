<?php

namespace Drupal\migrate_views\EventSubscriber;

use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Drupal\migrate_views\MigrateViewsPluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MigrationViewsSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $viewsPluginManager;

  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $displayPluginManager;

  /**
   * @var \Drupal\migrate_views\MigrateViewsPluginManager
   */
  protected $handlerPluginManager;

  /**
   * Creates a new MigrationViewsSubscriber instance.
   *
   * @param \Drupal\migrate_views\MigrateViewsPluginManager $viewsPluginManager
   * @param \Drupal\migrate_views\MigrateViewsPluginManager $displayPluginManager
   * @param \Drupal\migrate_views\MigrateViewsPluginManager $handlerPluginManager
   */
  public function __construct(MigrateViewsPluginManager $viewsPluginManager, MigrateViewsPluginManager $displayPluginManager, MigrateViewsPluginManager $handlerPluginManager) {
    $this->viewsPluginManager = $viewsPluginManager;
    $this->displayPluginManager = $displayPluginManager;
    $this->handlerPluginManager = $handlerPluginManager;
  }

  public function onRowSave(MigratePreRowSaveEvent $event) {
//    if ($event->getMigration()->id() !== 'd6_views') {
//      return;
//    }
//
//    $row = $event->getRow();
//    foreach (array_keys($this->viewsPluginManager->getDefinitions()) as $plugin_id) {
//      /** @var \Drupal\migrate_views\Plugin\migrate_views\ViewInterface $plugin */
//      $plugin = $this->viewsPluginManager->createInstance($plugin_id);
//      $plugin->transform($row->getDestination(), $event->ex)
//    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::PRE_ROW_SAVE][] = ['onRowSave'];
    return $events;
  }

}
