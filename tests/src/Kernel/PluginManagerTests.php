<?php

namespace Drupal\Tests\migrate_views\Kernel;

use Drupal\KernelTests\KernelTestBase;

class PluginManagerTests extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['migrate_views', 'migrate'];

  public function testPluginTypes() {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $view_level_pm */
    $view_level_pm = \Drupal::service('plugin.manager.migrate_views.migrate_views_view');
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $display_level_pm */
    $display_level_pm = \Drupal::service('plugin.manager.migrate_views.migrate_views_display');
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $handler_level_pm */
    $handler_level_pm = \Drupal::service('plugin.manager.migrate_views.migrate_views_handler');

    $definitions = $view_level_pm->getDefinitions();
    $this->assertArrayHasKey('base_table', $definitions);
  }

}
